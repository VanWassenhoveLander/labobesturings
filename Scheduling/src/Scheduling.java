import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Ruben
 * Implementation of the scheduling algorithms
 */
public class Scheduling {
    public static void fcfs(ArrayList<Process> processes) {
        int timer = 0;
        for(Process p : processes) {
            // If process arrives when processor is not busy
            // Set timer to arrivaltime of new process
            if(timer < p.arrivaltime) {
                timer = p.arrivaltime;
                p.waittime = 0;
            }
            // Else the process had to wait
            else {
                p.waittime = timer - p.arrivaltime;
            }
            p.starttime = timer;
            p.endtime = timer + p.servicetime;
            p.turnaroundtime = p.waittime + p.servicetime;
            p.normalizedTurnaroundtime = p.turnaroundtime / p.servicetime;
            timer = p.endtime;
        }
        System.out.println("breakpoint");
    }
    
    public static void roundRobin(ArrayList<Process> processes) {
        
    }
    
    public static void hrrn(ArrayList<Process> processes) {
        
    }
    public static void multilevelFeedback(ArrayList<Process> processes) {
        
    }
}

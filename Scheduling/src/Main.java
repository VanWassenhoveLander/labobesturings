import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.jfree.ui.RefineryUtilities;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Ruben
 */
public class Main {
    public static ArrayList<Process> processes= new ArrayList<Process>();
    public static void loadXML(){
        try {
            File file = new File("processen10000.xml");
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = (Document) db.parse(file);
            document.getDocumentElement().normalize ();
            System.out.println("Root element :" + document.getDocumentElement().getNodeName());
            NodeList processList = document.getElementsByTagName("process");
            for (int i=0; i<processList.getLength(); i++) {
                Element element = (Element)processList.item(i);
                int pid = Integer.parseInt(element.getElementsByTagName("pid").item(0).getTextContent());
                int arrivaltime = Integer.parseInt(element.getElementsByTagName("arrivaltime").item(0).getTextContent());
                int servicetime = Integer.parseInt(element.getElementsByTagName("servicetime").item(0).getTextContent());
                Process process = new Process(pid, arrivaltime, servicetime);
                processes.add(process);
            }
        }
        catch(Exception e) {
            System.out.println("An error occured.");
        }
    }

    public static void main(String[] args) {
        loadXML();
        Collections.sort(processes,Process.ProcArrivalTime);
        Scheduling.fcfs(processes);
        
        ShowResults chart = new ShowResults("servicetime vs waittime" , "chartTitle", processes);
        chart.pack();
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
        
    }
    
}

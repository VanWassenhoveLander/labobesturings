
import java.util.Comparator;

/**
 *
 * @author Ruben
 */
public class Process implements Comparable<Process>{
    int pid;
    int arrivaltime;
    int servicetime;
    int starttime;
    int endtime;
    int turnaroundtime;
    float normalizedTurnaroundtime;
    int waittime;
    
    public Process(int pid, int arrivaltime, int servicetime) {
        this.pid = pid;
        this.arrivaltime = arrivaltime;
        this.servicetime = servicetime;
        this.starttime = Integer.MAX_VALUE;
        this.endtime = Integer.MAX_VALUE;
        this.turnaroundtime = Integer.MAX_VALUE;
        this.normalizedTurnaroundtime = Float.MAX_VALUE;
        this.waittime = Integer.MAX_VALUE;
    }

    @Override
    public int compareTo(Process p) {
        return Integer.compare(this.pid, p.pid);
    }
    
    public static Comparator<Process> ProcArrivalTime = new Comparator<Process>() {

        @Override
	public int compare(Process p1, Process p2) {

	   int arrivaltime1 = p1.arrivaltime;
	   int arrivaltime2 = p2.arrivaltime;

	   /*For ascending order*/
	   return arrivaltime1-arrivaltime2;
    }};
    
    public static Comparator<Process> ProcServiceTime = new Comparator<Process>() {

        @Override
	public int compare(Process p1, Process p2) {

	   int servicetime1 = p1.servicetime;
	   int servicetime2 = p2.servicetime;

	   /*For ascending order*/
	   return servicetime1-servicetime2;
    }};
    
    
}

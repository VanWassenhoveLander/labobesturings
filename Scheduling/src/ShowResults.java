
import static java.lang.Math.floor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lander
 */
public class ShowResults extends ApplicationFrame{
    
   public static int [] percentielen = new int[100];
   
   public ShowResults( String applicationTitle , String chartTitle , ArrayList<Process> processes)
   {
      super(applicationTitle);
      JFreeChart lineChart = ChartFactory.createLineChart(
         chartTitle,
         "serviceTime","waittime",
         createDatasetnTAT(processes),
         PlotOrientation.VERTICAL,
         true,true,false);
         
      ChartPanel chartPanel = new ChartPanel( lineChart );
      chartPanel.setPreferredSize( new java.awt.Dimension( 560 , 367 ) );
      setContentPane( chartPanel );
      /*
      JFreeChart lineChart2 = ChartFactory.createLineChart(
         chartTitle,
         "serviceTime","nTAT",
         createDatasetnTAT(processes)
      );
      
      ChartPanel chartPanel2 = new ChartPanel( lineChart2 );
      chartPanel2.setPreferredSize( new java.awt.Dimension( 560 , 560 ) );
      setContentPane( chartPanel2 );*/
      
   }
    
    
    private DefaultCategoryDataset createDataset(ArrayList<Process> processes )
   {
      DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
      createPercentiles(processes,0);
      for(int i = 0; i < 100; i++){
          String serv = Integer.toString(i);
          dataset.addValue(percentielen[i], "waittime", serv);
      }
      
      return dataset;
   }
    
    private DefaultCategoryDataset createDatasetnTAT(ArrayList<Process> processes){
        
        DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
      
        createPercentiles(processes,1);
        for(int i = 0; i < 100; i++){
            String serv = Integer.toString(i);
            dataset.addValue(percentielen[i], "nTAT", serv);
        }
    
      
      return dataset;
    }
    
    private void createPercentiles(ArrayList <Process> list, int keuze){
        
        
        Collections.sort(list,Process.ProcServiceTime);
        int size = list.size(   );
        size = size/100;
        System.out.println("size="+size);
        int teller = 0;
        int som;
        if(keuze==0){
            while(teller < 100){
                som = 0;
                for(int i = teller*size; i < (teller*size)+size; i++){
                    som+=list.get(i).waittime;
                }
                percentielen[teller] = som/size;
                teller++;

            }
        }else{
            while(teller < 100){
                som = 0;
                for(int i = teller*size; i < (teller*size)+size; i++){
                    som+=list.get(i).normalizedTurnaroundtime;
                }
                percentielen[teller] = som/size;
                teller++;


            }
        }
    }
    
}
